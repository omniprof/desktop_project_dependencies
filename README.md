<h1>Desktop Project Dependencies Version 2.0</h1>

After cloning this project in NetBeans do a Run Maven -> Install to store this in your local repository where all other desktop projects in the omniprof gitlab repository that you clone will find it.

If you are not using NetBeans then it must be run with Maven with the goal install:install

The project contains the parent pom for all desktop and JavaFX/OpenFX sample programs.

Dependencies for JavaFX/OpenFX have been removed. Users of this parent should never have to alter it. It contains the dependencies for slf4j/log4j2 and JUnit 5 with legacy support.

Plugins consist of the maven-compiler-plugin, javafx-maven-plugin, and maven-surefire-plugin.

Properties define the compiler source and target as 14.

The javafx-maven-plugin depends on a property ${mainClass} that must be set in the local/child pom file.

The maven-surefire-plugin depends on a property ${skipTests} that must be set in the local/child pom file. While this might not be a good idea it does simplify turning off testing globally rather than place an @Ignore in every test class.

In the short term some of my samples will not work until their local pom is updated. 
